void main() {
  ///null safety
  var age = null;

  //check null
  if (age != null) {
    double ageDouble = age.toDouble();
    print(ageDouble);
  }

  // konversi null to nullable

  String name = 'gian';
  String? nullableName = name;

  int? nullableInt = null;

  if (nullableInt != null) {
    int harga = nullableInt;
  }

  //defaultValue
  String? guest;

  var guestName = guest ?? 'Tamu';

  // konversi paksa
  int? nullableNumber;
  nullableNumber = 10;
  int nonNullableNumber = nullableNumber;

  print(nonNullableNumber);

  //mengakses nullable member
  int? intNumber;

  double? intToDouble = intNumber?.toDouble();
  print(intToDouble);

  ///For Loop
  for (var i = 1; i <= 10; i++) {
    print('Perulangan i ke $i');
  }

  ///while

  var a = 1;

  while (a <= 10) {
    print('Perulangan a ke-$a');

    a++;
  }

  /// Do While
  var counter = 100;

  do {
    print('Perulangan counter ke-$counter');
    counter++;
  } while (counter <= 10);

  /// Break
  var hitung = 1;

  while (true) {
    print('Perulangan hitung ke $hitung');
    hitung++;

    if (hitung > 10) {
      break;
    }
  }

  /// Continue
  for (var ulang = 1; ulang <= 10; ulang++) {
    if (ulang % 2 == 0) {
      continue;
    }
    print('Looping continue ke $ulang');
  }

  ///For In
  var arraySaya = <String>['Gian', 'Athaya', 'Zayd', 'Zaynab', 'Zayn'];
  var nameSet = <String>{'Gian', 'Gia', 'Ghea', 'Gunawan', 'Gunadi'};

  for (var value in arraySaya) {
    print(value);
  }

  for (var nama in nameSet) {
    print(nama);
  }

  /// Function & function parameter & optional Parameter
  void sayHello(String firstName, String secondName,
      [String? middleName = 'Jago']) {
    print('Hello, $firstName $middleName $secondName');
  }

  sayHello('Gian', 'Pratama', 'The Sword');
  sayHello('Angga', 'Budiman');

  ///Named parameter
  void dataTim(
      {required String? namaTim,
      required String? ketua,
      required String? sekretaris,
      required String? bendahara,
      required int? noUrut}) {
    print(
        'Keterangan Tim $namaTim => Ketua: $ketua, Sekretaris: $sekretaris, Bendahara: $bendahara, No. Urut: $noUrut');
  }

  dataTim(
      ketua: 'Gian',
      sekretaris: 'Athaya',
      bendahara: 'Zaynab',
      namaTim: 'The Cord',
      noUrut: 13);

  dataTim(
      namaTim: 'The Caravan',
      ketua: 'Agus',
      sekretaris: 'Jupri',
      bendahara: 'Nurul',
      noUrut: 14);

  /// Function return value
  int sum(List<int> numbers) {
    var total = 0;

    for (var nilai in numbers) {
      total += nilai;
    }
    return total;
  }

  print(sum([10, 20, 30]));

  String nama(String name) {
    return 'Hello $name';
  }

  var data = nama('Athaya');
  print(data);

  /// Function Short Expression
  int pengurangan(int satu, int dua) => satu - dua;

  print(pengurangan(50, 20));

  /// Higher order function
  void halo(String name, String Function(String) filter) {
    var filteredName = filter(name);

    print('Hello $filteredName');
  }

  String filterBadWord(String name) {
    if (name == 'gila') {
      return '****';
    } else {
      return name;
    }
  }

  halo('gian', filterBadWord);
  halo('gila', filterBadWord);

  /// Anonymous Function

  var upper = (String name) => name.toUpperCase();
  var lower = (String name) => name.toLowerCase();

  print(upper('gian'));
  print(lower('ATHAYA'));

  void hello(String name, String Function(String) filter) {
    print('Hello, ${filter(name)}');
  }

  hello('Gian', (name) => name.toUpperCase());

  /// Scope

  var user = 'Gian';

  void call() {
    var init = 'Hello $user';
    print(init);
  }

  call();

  /// Closure
  var count = 0;

  void increment() {
    print('Increment');
    count++;
  }

  print(count);
  increment();
  increment();
  print(count);

  /// Recursive function
  int factorialLoop(int value) {
    var result = 1;

    for (var i = 1; i <= value; i++) {
      result *= i;
    }

    return result;
  }

  int factorialRecursive(int value) {
    if (value == 1) {
      return 1;
    } else {
      return value * factorialLoop(value - 1);
    }
  }

  print(factorialLoop(10));
  print(factorialRecursive(10));

  void loop(int value) {
    if (value == 0) {
      print('selesai');
    } else {
      print('Perulangan ke $value');
      loop(value - 1);
    }
  }

  loop(100);
}
