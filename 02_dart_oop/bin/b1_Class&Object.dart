///Membuat Class dalam OOP
class Person {
  //Field
  String nama = 'Gunadi';
  String? alamat;
  final String negara = 'Indonesia';

  //Method
  String sayHello(String paramName) {
    return 'Halo, $paramName. Saya $nama! Senang berkenalan dengan Anda';
  }

  // Method Expression Body
  String tanyaAlamat(String paramName) =>
      'Halo, $paramName. Alamat rumah kamu dimana?';
}

//Extension Method
extension SayGoodByeOnPerson on Person {
  String sayGoodBye(String paramName) =>
      'Well done $paramName. See you when I see you! Regards: $nama from $negara';
}

void main() {
  //Membuat object dalam OOP
  Person orang1 = Person();
  var orang2 = Person();

  //Mengubah field dari object
  orang1.nama = 'Gian';
  orang2.nama = 'Athaya';

  print(orang1.nama);
  print(orang1.sayHello('Huda'));
  print(orang1.tanyaAlamat('Arimbi'));
  print(orang1.sayGoodBye('Arinda'));
}
