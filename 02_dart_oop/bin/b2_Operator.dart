class Orange {
  int jumlah = 0;

  Orange operator +(Orange orange) {
    var result = Orange();

    result.jumlah = jumlah + orange.jumlah;
    return result;
  }
}

void main() {
  var orange1 = Orange();
  var orange2 = Orange();

  orange1.jumlah = 15;
  orange2.jumlah = 8;

  var orange3 = orange1 + orange2;
  print(orange3.jumlah);
}
