///Membuat Class dalam OOP
class Person {
  //Field
  String nama = 'Guest';
  String? alamat;
  String negara = 'Default';

  // Constructor, Variable Shadowing, This, Initializing Formal Param
  Person(this.nama, this.alamat, this.negara);

  //Named Constructor
  Person.withNama(this.nama);
  Person.withAlamat(this.alamat, this.negara);

  //Redirecting Constructor (& with named constructor)
  Person.withNegara(String negara) : this("", "", negara);
  Person.fromJakarta() : this.withAlamat('Kupang', 'Indonesia');

  //Method
  String sayHello(String paramName) {
    return 'Halo, $paramName. Saya $nama! Senang berkenalan dengan Anda';
  }

  // Method Expression Body
  String tanyaAlamat(String paramName) =>
      'Halo, $paramName. Alamat rumah kamu dimana?';
}

class Customer {
  String fullName = '';
  String firstName = '';
  String lastName = '';

  //Initializer List : tidak terhalang oleh variable shadowing
  Customer(this.fullName)
      : firstName = fullName.split(" ")[0],
        lastName = fullName.split(" ")[1] {
    print('Create new Customer:');
  }
}

class ImmutablePoint {
  final int nilaiX;
  final int nilaiY;

  // Constant Constructor untuk immutable data, dan variabel harus diset final.
  const ImmutablePoint(this.nilaiX, this.nilaiY);
}

class Database {
  Database() {
    print('Create new database connection');
  }

  static Database database = Database();

  factory Database.get() {
    return database;
  }
}

class User {
  String? username;
  String? name;
  String? email;
}

User? createUser() {
  return null;
}

//Extension Method
extension SayGoodByeOnPerson on Person {
  String sayGoodBye(String paramName) =>
      'Well done $paramName. See you when I see you! Regards: $nama from $negara';
}

void main() {
  //Membuat object dalam OOP
  Person orang1 = Person('Eko', 'Archon', 'Alaska');

  var orang2 = Person('Rudi', 'Tokyo', 'Japan');

  Person orang3 = Person.withNama('Jayen');

  var orang4 = Person.withAlamat('Mandalika', 'Indonesia');

  Person orang5 = Person.fromJakarta();

  // Object Constant Constructor
  var ponten = const ImmutablePoint(10, 5);
  var angka = const ImmutablePoint(10, 5);

  //Mengubah field dari object
  // orang1.nama = 'Gian';
  // orang2.nama = 'Athaya';

  print(orang1.nama);
  print(orang3.sayHello('Huda'));
  print(orang2.tanyaAlamat('Arimbi'));
  print(orang2.sayGoodBye('Arinda'));
  print(orang5.nama);

  var customer = Customer('Gian Pratama');

  print(customer.firstName);
  print(customer.lastName);
  print(customer.fullName);

  print(ponten == angka);

  //implementasi factory constructor
  var data1 = Database.get();
  var data2 = Database.get();
  print(data1 == data2);

  //tanpa cascade notation

  //var user = User();
  // user.username = 'mistergambit';
  // user.name = 'Gian Pratama';
  // user.email = 'mrgambit@explore.org';

  //dengan Cascade Notation
  var user = User()
    ..username = 'mistergambit'
    ..name = 'Gian Pratama'
    ..email = 'mrgambit@explore.org';

  User? user2 = createUser()
    ?..username = 'mistergambit'
    ..name = 'Gian Pratama'
    ..email = 'mrgambit@explore.org';
}
